# Estenografía por Enmascaramiento con Eco

Este repositorio contiene el desarrollo del primer proyecto del curso
de Procesamiento Digital de Señales de la Maestría en Electrónica del
ITCR.

# HOW TO

Ejecutarlo en modo de codificacion:
`./main.py --in_audio ~/Music/in.wav --out_audio ~/Music/out.wav --metadata_file metadata.json`

Ejecutarlo en modo de decodificacion:
`./main.py --in_audio ~/Music/out.wav`

Ejecutarlo codificando en el canal derecho:
`./main.py --in_audio ~/Music/in.wav --out_audio ~/Music/out.wav --metadata_file metadata.json --right_channel`

Ejecutarlo codificando en el canal izquierdo:
`./main.py --in_audio ~/Music/in.wav --out_audio ~/Music/out.wav --metadata_file metadata.json --left_channel`

Ejecutarlo decodificando en el canal derecho:
`./main.py --in_audio ~/Music/out.wav --right_channel`

Ejecutarlo decodificando en el canal izquierdo:
`./main.py --in_audio ~/Music/out.wav --left_channel`

Ejecutarlo en modo de codificacion con corrección de errores:
`./main.py --in_audio ~/Music/in.wav --out_audio ~/Music/out.wav --metadata_file metadata.json --encoded`

Ejecutarlo en modo de decodificacion con corrección de errores:
`./main.py --in_audio ~/Music/in.wav --out_audio ~/Music/out.wav --metadata_file metadata.json --encoded`

Ejecutar codificacion con paramatros distintos al default:
`./main.py --in_audio ~/Music/in.wav --out_audio ~/Music/out.wav --metadata_file metadata.json --a_0 0.4 --a_1 0.7 --t_0 13 --t_1 23 --window_size 50`

Ejecutarlo decodificacion con paramatros distintos al default:
`./main.py --in_audio ~/Music/out.wav --t_0 13 --t_1 23 --window_size 50`

Ejecutarlo en modo de prueba (codificacion y decodificacion realizada en el mismo proceso, obtiene estadísticas:
`./main.py --test_mode --in_audio ~/Music/in.wav --metadata_file metadata.json`

Ejecutarlo en modo de prueba con valores de rudio como entrada **Recuperación del 95%**:
`./main.py --test_mode --metadata_file metadata.json`

Ejecutarlo en modo de prueba con valores de rudio como entrada y señal :
`./main.py --test_mode`

Ejecutarlo en modo de prueba con correccion de errores:
`./main.py --test_mode --encoded`

Obtener ayuda:
`./main.py --help`
`./main.py -h`
`./main.py`

## Requerimientos

* Python 3
** matplotlib
** numpy
** bistring
** reedsolo

Estos requerimientos pueden ser instalados utilizando pip3

## Running

### Install PEAQ
* Inicialice los submódulos `git init submodules`
* Configure el proyecto:
** `./autogen.sh`
** `sudo ./autogen.sh  --prefix /usr/ --libdir /usr/lib/x86_64-linux-gnu/`
** `make CFLAGS=-Wno-error`
** `sudo make install`

# TODO

## Ambos

- [X] Extracción de Audio
- [X] Lectura de parámetros
- [X] Enventanado
- [X] Convolucionador
- [X] Seleccion del canal (derecha o izquierda)

## Codificador

- [ ] Lectura de Metadatos
- [X] Multiplexador de datos
- [X] Combinación de ventanas (overlap and sum || overlap and add)
- [X] Escritura de Audio

## Decodificador

- [X] Autocorrelacionador del Cepstro
- [X] Clasificador
- [ ] Interpretador de datos
- [ ] Notificacion de Metadatos


## Paper
 - [X] Archivo LaTeX
 - [X] Abstract
 - [X] Palabras Claves
 - [X] Introduccion
 - [ ] Estado del Arte?
 - [X] Solución propuesta
 - [X] Análisis de Resultados
  - [X] Tasa de Recuperación
  - [X] PESQ
  - [ ] Tasa de transferencia de metadados vs calidad perceptual (PESQ_??)
  - [X] Distintos tipos de géneros musicals, ruido, señales constantes, señales senoidales
  - [ ] Otros resultados
 - [X] Conclusiones
 - [X] Citas bibliograficas

## Opcional
 - [ ] Codificación de la imágen de la portada del disco
 - [X] Sistema de Corrección de errores
 - [ ] Ejecución del sistema en linea
