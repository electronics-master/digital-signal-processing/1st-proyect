#!/usr/bin/python3

import matplotlib.pyplot as plt
import argparse
from bitstring import Bits
import configparser
import numpy as np
import random
import string
import sys
import wave

from decoder import decoder
from encoder import encoder
from entropy import entropy
from error_correction import ecc_encode, ecc_decode
from json_parser import read_json_from_file, string_to_json
from utils import message_to_bytes, bytes_to_msg

# Coder-Decoder Params
AMPLITUDE_1 = 0.4
DELAY_1 = 7
AMPLITUDE_2 = 0.4
DELAY_2 = 13
NCHANNELS = 2
WINDOW_SIZE = 200

# Audio Params, used for the test mode
SAMPLE_RATE = 44100
NUM_CHANNELS = 2
SAMPLE_WIDTH = 2 # In bytes
IN_AUDIO_TMP = "/tmp/in_audio.wav"
OUT_AUDIO_TMP = "/tmp/out_audio.wav"
MSG_LENGHT = 100

def check_positive_int(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

def cmdline_args():
    # Make parser object
    p = argparse.ArgumentParser(
        description="""
        Echo Masking Stenography

        This is program executes an echo masking stenography to encode
        a message into an 44100Hz, 16bit stereo wav file """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    p.add_argument("--in_audio", type=str,
                   help="Input audio filename")
    p.add_argument("--out_audio", type=str,
                   help="Output audio filename")
    p.add_argument("--metadata_file", type=str,
                   help="Metadata json file, this argument is optional for \
                         test mode. A sample one is provided in metadata.json")
    p.add_argument("--a_0", type=float,
                   help="Amplitude for the 0 bit echo", default=AMPLITUDE_1)
    p.add_argument("--t_0", type=check_positive_int,
                   help="Delay for the 0 bit echo", default=DELAY_1)
    p.add_argument("--a_1", type=float,
                   help="Amplitude for the 1 bit echo", default=AMPLITUDE_2)
    p.add_argument("--t_1", type=check_positive_int,
                   help="Delay for the 1 bit echo", default=DELAY_2)
    p.add_argument("--window_size", type=check_positive_int,
                   help="Window size", default=WINDOW_SIZE)

    group1 = p.add_mutually_exclusive_group(required=False)
    group1.add_argument('--right_channel', action="store_true")
    group1.add_argument('--left_channel', action="store_false")

    group2 = p.add_mutually_exclusive_group(required=False)
    group2.add_argument('--test_mode', action="store_true",
        help="""If set the system will encode and decode the signal
            without saving it to disk. It will also compare the input bits with
            the output bits""")
    
    group3 = p.add_mutually_exclusive_group(required=False)
    group3.add_argument('--encoded', action="store_true",
        help="""If set the system will encoded the message using
        Reed-Solomon encoding in packages of 2 bytes with 2 extra bits.""")

    return p


if __name__ == '__main__':

    if sys.version_info < (3, 0, 0):
        sys.stderr.write("You need python 3.0 or later to run this script\n")
        sys.exit(1)

    p = cmdline_args()

    try:
        args = p.parse_args()
    except BaseException:
        sys.exit(1)

    if(args.t_0 == args.t_1):
        print("Delay parameters must be different")
        sys.exit(1)
        
    if(args.in_audio):
        # Get message
        in_wave = wave.open(args.in_audio, 'rb')
        nchannels = in_wave.getsampwidth()
        swidth = in_wave.getsampwidth()
        rate = in_wave.getframerate()
        input_signal = np.frombuffer(in_wave.readframes(-1), dtype=np.short)

        data_per_channel = [input_signal[offset::nchannels] for offset in range(nchannels)]

        if(args.right_channel):
            print("right_channel")
            encoded_channel = data_per_channel[1]
            clean_channel = data_per_channel[0]
        else:
            encoded_channel = data_per_channel[0]
            clean_channel = data_per_channel[1]

    if(not args.test_mode):
        if(not args.in_audio):
            print("Error: Please add an input signal or use it in test mode")
            p.print_help()
            sys.exit(1)
            
        if(not args.metadata_file):
            print("Error: Please add a metadata file")
            p.print_help()
            sys.exit(1)

            
        if(args.out_audio):
            print("Operating in encoding mode")

            json = read_json_from_file(args.metadata_file)
            json_string = string_to_json(json)

            byte_msg = message_to_bytes(json)

            # Add zero at the end to mark end of the message
            byte_msg += b'\0'

            if(args.encoded):
                byte_msg = encode(byte_msg)

            encoded_signal = encoder(encoded_channel, byte_msg, args)

            # Reinterleave channels
            clean_channel = np.append(clean_channel, np.zeros(len(encoded_signal) - len(clean_channel), dtype=np.short))
            if(args.right_channel):
                full_encoded = np.vstack((clean_channel, encoded_signal)).reshape((-1,),order='F')
            else:
                full_encoded = np.vstack((encoded_signal, clean_channel)).reshape((-1,),order='F')


            data_per_channel = [full_encoded[offset::nchannels] for offset in range(nchannels)]

            out_wave = wave.open(args.out_audio, 'wb')
            out_wave.setnchannels(nchannels)
            out_wave.setsampwidth(swidth)
            out_wave.setframerate(rate)
            out_wave.writeframes(full_encoded)
            out_wave.close()

        else:
            print("Operating in decoding mode")

            encoded_signal = encoded_channel

            decoded_bytes = decoder(encoded_signal, args)

            print(decoded_bytes)
    else:
        print("Operating in test mode")
        if(args.metadata_file):
            json = read_json_from_file(args.metadata_file)
        else:
            if(args.in_audio):
                msg_length = int(len(encoded_channel)/args.window_size)
            else:
                msg_length = MSG_LENGHT

            json = ''.join(random.choices(string.ascii_uppercase + string.digits, k=msg_length))

        byte_msg = message_to_bytes(json)
        # Add zero at the end to mark end of the message
        byte_msg += b'\0'
        if(args.encoded):
            byte_msg = ecc_encode(byte_msg)

        if(not args.in_audio):
            print("Missing input audio data, using white noise sample")
            nchannels = NCHANNELS
            swidth = SAMPLE_WIDTH
            rate = SAMPLE_RATE
            
            encoded_channel = np.random.normal(0, 1, len(byte_msg)*8*args.window_size)
            clean_channel = np.random.normal(0, 1, len(encoded_channel))

            full_original = np.vstack((clean_channel, encoded_channel)).reshape((-1,),order='F')

            # Save the sample so that it can be evaluated with PESQ
            in_wave = wave.open(IN_AUDIO_TMP, 'wb')
            in_wave.setnchannels(nchannels)
            in_wave.setsampwidth(swidth)
            in_wave.setframerate(rate)
            in_wave.writeframes(full_original)
            in_wave.close()

        # print("signal entropy: ", entropy(encoded_channel))

        encoded_signal = encoder(encoded_channel, byte_msg, args)

        decoded_bytes = decoder(encoded_signal, args)
        if(args.encoded):
            decoded_bytes = ecc_decode(decoded_bytes)
        
        clean_channel = np.append(clean_channel, np.zeros(len(encoded_signal) - len(clean_channel), dtype=np.short))
        full_encoded = np.vstack((clean_channel, encoded_signal)).reshape((-1,),order='F')
            
        # Save the sample so that it can be evaluated with PESQ
        if(not args.out_audio):
            out_audio_name = OUT_AUDIO_TMP
        out_wave = wave.open(out_audio_name, 'wb')
        out_wave.setnchannels(nchannels)
        out_wave.setsampwidth(swidth)
        out_wave.setframerate(rate)
        out_wave.writeframes(full_encoded)
        out_wave.close()

        in_bits = Bits(message_to_bytes(json))
        out_bits = Bits(decoded_bytes)

        correct = 0
        incorrect = 0

        for in_bit, out_bit in zip(in_bits, out_bits):
            if(in_bit == out_bit):
                correct += 1
            else:
                incorrect += 1

        if(incorrect + correct != 0):
            print("Bit recovery: ", correct / (incorrect + correct))

        in_bytes = message_to_bytes(json)
        out_bytes = decoded_bytes

        correct = 0
        incorrect = 0

        

        for in_byte, out_byte in zip(in_bytes, out_bytes):
            if(in_byte == out_byte):
                correct += 1
            else:
                incorrect += 1
        
        if(incorrect + correct != 0):
            print("Byte recovery: ", correct / (incorrect + correct))

        print(in_bytes)
        print(decoded_bytes)
