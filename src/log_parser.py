#!/bin/python3

import sys
import re
import csv


lines = []
with open("run1.log", "r") as file:
    for line in file:
        lines.append(line)
# with open("run2.log", "r") as file:
#     for line in file:
#         lines.append(line)
# with open("run3.log", "r") as file:
#     for line in file:
#         lines.append(line)
# with open("run4.log", "r") as file:
#     for line in file:
#         lines.append(line)
# with open("run5.log", "r") as file:
#     for line in file:
#         lines.append(line)

atten_dict = {}

#Parse encoding and encoding 
for i in range(len(lines)):
    if "a_0:" in lines[i]:
        at_reg = re.search("([0-9]\.[0-9]+).+([0-9]\.[0-9]+)", lines[i])
        # ent_reg = re.search("signal entropy: ([0-9]\.[0-9]+)", lines[i+1])
        bit_reg = re.search("Bit recovery: +([0-9]\.[0-9]+)", lines[i+1])
        byte_reg = re.search("Byte recovery: +([0-9]\.[0-9]+)", lines[i+2])
        dist_reg = re.search("Grade: +(-*[0-9]\.[0-9]+)", lines[i+3])

        atten = float(at_reg.groups()[0])
        
        try:
            atten_dict[atten]["count"] += 1
            #atten_dict[atten]["ent"] += float(ent_reg.groups()[0])
            atten_dict[atten]["bit"] += float(bit_reg.groups()[0])
            atten_dict[atten]["byte"] += float(byte_reg.groups()[0])
            atten_dict[atten]["peaq"] += float(dist_reg.groups()[0])
        except KeyError:
            atten_dict[atten] = {"count": 1}
            #atten_dict[atten]["ent"] = float(ent_reg.groups()[0])
            atten_dict[atten]["bit"] = float(bit_reg.groups()[0])
            atten_dict[atten]["byte"] = float(byte_reg.groups()[0])
            atten_dict[atten]["peaq"] = float(dist_reg.groups()[0])

delay_dict = {}

#Parse delays
for i in range(len(lines)):
    if "t_0:" in lines[i]:
        del_reg = re.search("t_0: ([0-9]+)", lines[i])
        # ent_reg = re.search("signal entropy: ([0-9]\.[0-9]+)", lines[i+1])
        bit_reg = re.search("Bit recovery: +([0-9]\.[0-9]+)", lines[i+1])
        byte_reg = re.search("Byte recovery: +([0-9]\.[0-9]+)", lines[i+2])
        dist_reg = re.search("Grade: +(-*[0-9]\.[0-9]+)", lines[i+3])

        if None in [del_reg, bit_reg, byte_reg, dist_reg]:
            continue
        delay = float(del_reg.groups()[0])
        try:
            delay_dict[delay]["count"] += 1
            #delay_dict[delay]["ent"] += float(ent_reg.groups()[0])
            delay_dict[delay]["bit"] += float(bit_reg.groups()[0])
            delay_dict[delay]["byte"] += float(byte_reg.groups()[0])
            delay_dict[delay]["peaq"] += float(dist_reg.groups()[0])
        except KeyError:
            delay_dict[delay] = {"count": 1}
            #delay_dict[delay]["ent"] = float(ent_reg.groups()[0])
            delay_dict[delay]["bit"] = float(bit_reg.groups()[0])
            delay_dict[delay]["byte"] = float(byte_reg.groups()[0])
            delay_dict[delay]["peaq"] = float(dist_reg.groups()[0])

window_dict = {}

#Parse window_size
for i in range(len(lines)):
    if "window size:" in lines[i]:
        window_reg = re.search("window size: +([0-9]+)", lines[i])
        # ent_reg = re.search("signal entropy: ([0-9]\.[0-9]+)", lines[i+1])
        bit_reg = re.search("Bit recovery: +([0-9]\.[0-9]+)", lines[i+1])
        byte_reg = re.search("Byte recovery: +([0-9]\.[0-9]+)", lines[i+2])
        dist_reg = re.search("Grade: +(-*[0-9]\.[0-9]+)", lines[i+3])
        
        window = float(window_reg.groups()[0])
        try:
            window_dict[window]["count"] += 1
            #window_dict[window]["ent"] += float(ent_reg.groups()[0])
            window_dict[window]["bit"] += float(bit_reg.groups()[0])
            window_dict[window]["byte"] += float(byte_reg.groups()[0])
            window_dict[window]["peaq"] += float(dist_reg.groups()[0])
        except KeyError:
            window_dict[window] = {"count": 1}
            #window_dict[window]["ent"] = float(ent_reg.groups()[0])
            window_dict[window]["bit"] = float(bit_reg.groups()[0])
            window_dict[window]["byte"] = float(byte_reg.groups()[0])
            window_dict[window]["peaq"] = float(dist_reg.groups()[0])

atten_csvfile = open("attenuation.csv", "w")
atten_csvwriter = csv.writer(atten_csvfile)

atten_csvwriter.writerow(["atten", "bit", "byte", "peaq"])
for atten in atten_dict:
    count = atten_dict[atten]["count"]
    bit = atten_dict[atten]["bit"] / count
    byte = atten_dict[atten]["byte"] / count
    peaq = atten_dict[atten]["peaq"] / count
    atten_csvwriter.writerow([atten, bit, byte, peaq])


window_csvfile = open("window_size.csv", "w")
window_csvwriter = csv.writer(window_csvfile)

window_csvwriter.writerow(["window", "bit", "byte", "peaq"])
for window in window_dict:
    count = window_dict[window]["count"]
    bit = window_dict[window]["bit"] / count
    byte = window_dict[window]["byte"] / count
    peaq = window_dict[window]["peaq"] / count
    window_csvwriter.writerow([window, bit, byte, peaq])


delay_csvfile = open("delay.csv", "w")
delay_csvwriter = csv.writer(delay_csvfile)

delay_csvwriter.writerow(["delay", "bit", "byte", "peaq"])
for delay in delay_dict:
    count = delay_dict[delay]["count"]
    bit = delay_dict[delay]["bit"] / count
    byte = delay_dict[delay]["byte"] / count
    peaq = delay_dict[delay]["peaq"] / count
    delay_csvwriter.writerow([delay, bit, byte, peaq])
