import numpy as np


def windows_combiner(window_size, convolved_windows, filter_size):
    """Performs the overlap and add operation

    window_size: size of the window in the original vector
    windows: list containing all windows to be operated with
    filter_size: size of the filter applied to the windows in window

    return a single array with the resulting data
    """
    output_array = np.zeros(
        len(convolved_windows) *
        window_size +
        filter_size -
        1)
    new_window_size = window_size + filter_size - 1
    address = 0

    for window in convolved_windows:
        output_array[address:address + new_window_size] += window
        address += window_size

    return output_array
