import numpy as np

from utils import divide_array, bits
from window_combiner import windows_combiner

def encoder(signal, byte_msg, params):
    data_type = signal.dtype

    # Get filters for convolution
    F_0, F_1, F_2 = convolution_filter(
        params.a_0, params.t_0, params.a_1, params.t_1)

    windows = divide_array(signal, params.window_size)

    bit_msg = bits(byte_msg)

    # Ensure that both the message and the signal have corresponding sizes
    if(len(windows) > len(bit_msg)):
        # Append zeros to the end of the message
        bit_msg += "0" * (len(windows) - len(bit_msg))
    elif(len(windows) < len(bit_msg)):
        # Append zero windows at the end of the message
        zero_window = np.ones(params.window_size)

        windows.extend(np.repeat([zero_window], len(bit_msg) -
                                 len(windows), axis=0))

    convolved_windows = []
    # Parse all bits of the message
    for bit, window in zip(bit_msg, windows):
        if(bit == '1'):
            convolved_windows.append(np.convolve(window, F_1))
        elif(bit == '0'):
            convolved_windows.append(np.convolve(window, F_0))
        else:
            convolved_windows.append(np.convolve(window, F_2))

    output = windows_combiner(params.window_size, convolved_windows, len(F_0))

    return output.astype(data_type)


def convolution_filter(a_0, t_0, a_1, t_1):
    # output size is N + M - 1
    # M is max(t_0, t_1)
    # N = AUDIO_SIZE / len(MESSAGE)
    filter_size = max(t_1, t_0) + 1

    F_0 = np.zeros(filter_size)
    F_0[t_0] = a_0
    F_0[0] = 1

    F_1 = np.zeros(filter_size)
    F_1[t_1] = a_1
    F_1[0] = 1

    F_2 = np.zeros(filter_size)
    F_2[0] = 1

    return F_0, F_1, F_2
