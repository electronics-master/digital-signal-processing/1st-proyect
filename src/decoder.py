from bitstring import Bits
import numpy as np

from utils import divide_array, cepstrum

import matplotlib.pyplot as plt

def decoder(encoded_signal, params):
    windows = divide_array(encoded_signal, params.window_size)
    output_data = ""

    for i, window in enumerate(windows):
        correlated_window = cepstrum(window)

        peak1 = correlated_window[params.t_1]
        peak0 = correlated_window[params.t_0]

        if (peak1 > peak0):
            output_data += "1"
        else:
            output_data += "0"

    out_bytes = Bits(bin=output_data).tobytes()

    if(b'\0' in out_bytes):
        zero_index = out_bytes.index(b'\0')
        return out_bytes[:zero_index]
    else:
        return out_bytes
        
