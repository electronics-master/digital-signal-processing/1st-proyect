import json


def remove_whitespace(string):
    return ''.join(string.split())


def read_json_from_file(filename):
    with open(filename, 'r') as fp:
        data = fp.read()
    return remove_whitespace(data)


def string_to_json(string):
    return json.loads(string)
