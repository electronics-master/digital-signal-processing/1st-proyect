import numpy as np


def entropy(signal):
    # Based on the matlab implementation of Gonzales Woods entropy
    hist = np.histogram(signal)[0]
    norm_hist = hist/sum(hist) + 1e-10 # Last value added to avoid log(0)
    log2 = np.log2(norm_hist)
    entropy = -1*sum(norm_hist*log2)
    
    return entropy
