#!/bin/bash

LC_NUMERIC=en_US.UTF-8 # Allows seq to output decimal point
IN_AUDIO=../data/16KHz.wav
OUT_AUDIO=../data/out.wav
DIST_ANALIZER=peaq
DIST_STRING="Objective Difference Grade"

# Inputs AUDIO1, white noise?, 
# Outputs: time, pesq, bit precission & byte precission


# Change Attenuation, white noise
echo "Testing attenuation changes with white noise input and t_0->7, t->13, window_size = 200, no encoding"
for i in $(seq 0 0.05 1)
do
    echo "a_0: $i a_1: $i"
    ./main.py --test_mode --a_0 $i --a_1 $i --encoded | grep -e recovery -e entropy
    if [ $? -eq 0 ]; then
	$DIST_ANALIZER /tmp/in_audio.wav /tmp/out_audio.wav
    fi
done
echo

# Change Delay, white noise, a_0 and a_1 -> 0
echo "Testing delays changes with white noise input and a_0->1, a_1->1, window_size=200, no encoding"
for i in $(seq 1 5 15)
do
    j=31
    echo "t_0: $i t_1: $j"
    ./main.py --test_mode --t_0 $i --t_1 $j | grep recovery
    if [ $? -eq 0 ]; then
	$DIST_ANALIZER /tmp/in_audio.wav /tmp/out_audio.wav
    fi
done
echo

# Change window size
echo "Testing window size changes with white noise input and a_0->1, a_1->1, t_0->t, t->13, no encoding"
for i in $(seq 15 30 200)
do
    echo "window size: $i"
    ./main.py --test_mode --window_size $i | grep recovery
    if [ $? -eq 0 ]; then
	$DIST_ANALIZER /tmp/in_audio.wav /tmp/out_audio.wav
    fi
done
echo

# # Change Encoding, same as changes in attenuation but with encoding
# echo "Same arguments as attenuation modifications, but with encoding"
# for i in $(seq 0 0.1 1)
# do
#     echo "a_0: $i a_1: $i"
#     ./main.py --test_mode --a_0 $i --a_1 $i --encoded | grep recovery
#     if [ $? -eq 0 ]; then
# 	$DIST_ANALIZER /tmp/in_audio.wav /tmp/out_audio.wav
#     fi
# done
# echo


# Change input audio, measure input entropy???

