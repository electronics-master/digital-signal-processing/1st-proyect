import reedsolo as reedsolo

DATA_BYTES = 4
ENCODING_BYTES = 4
rs = reedsolo.RSCodec(ENCODING_BYTES)


def ecc_encode(byte_msg):
    return_bytes = b''

    while(len(byte_msg) != 0):
        return_bytes += rs.encode(byte_msg[:DATA_BYTES])
        byte_msg = byte_msg[DATA_BYTES:]
    return return_bytes


def ecc_decode(encoded_bytes):
    return_bytes = b''

    while(len(encoded_bytes) != 0):
        try:
            return_bytes += rs.decode(encoded_bytes[:(DATA_BYTES+ENCODING_BYTES)])
        except reedsolo.ReedSolomonError:
            return_bytes += encoded_bytes[:DATA_BYTES]
        encoded_bytes = encoded_bytes[(DATA_BYTES+ENCODING_BYTES):]
    return return_bytes
    
