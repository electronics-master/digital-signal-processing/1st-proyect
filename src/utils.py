import numpy as np
from bitstring import Bits, BitArray


def divide_array(array, element_size):
    output_list = []

    # Determine next lenght divisible by element size, this is done to
    # get equal sized elements
    if(len(array) % element_size == 0):
        new_length = len(array)
    else:
        new_length = (len(array) // element_size + 1) * element_size

    # Add zeros to match new_length
    array = np.pad(array, (0, (new_length - len(array))), 'constant')

    # Return equal sized arrays
    array = np.array_split(array, new_length / element_size)

    return array


def replaceZeroes(data):
    min_nonzero = np.min(data[np.nonzero(data)])
    data[data == 0] = min_nonzero
    return data


def cepstrum(x):
    # Only process if array is different from all zeroes
    if(np.any(x)):
        spectrum = np.fft.fft(x)
        abs = np.abs(spectrum)
        abs = replaceZeroes(abs)
        log = np.log(abs)
        ceps = np.fft.ifft(log).real
    else:
        return x

    return ceps


def normalize(array):
    return (array - np.min(array)) / np.ptp(array)


def message_to_bytes(message):
    return bytes(message, "ascii")


def bytes_to_msg(byte):
    return byte.decode('ascii')


def bits(byte):
    return Bits(byte).bin
